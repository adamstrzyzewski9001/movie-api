require('dotenv').config()
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../index')
const { expect } = chai

chai.use(chaiHttp)

describe('/GET comments', () => {
  it('it should GET all the comments', done => {
    chai
      .request(app)
      .get('/comments')
      .end((err, res) => {
        if (err) done(err)
        expect(res).to.have.status(200)
        expect(res.body.comments).to.be.an('array')
        done()
      })
  })
})

describe('/POST comments', () => {
  it('it should create a comment for a movie with imdbid: tt0106062', done => {
    chai
      .request(app)
      .post('/comments')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send({
        imdbId: 'tt0106062',
        author: 'Authors name (test)',
        text: 'A test comment'
      })
      .end((err, res) => {
        if (err) done(err)
        expect(res.status).to.be.eq(201)
        expect(res.body.comment).to.be.an('object')
        done()
      })
  })
})
