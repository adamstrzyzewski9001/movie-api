require('dotenv').config()
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../index')
const { expect } = chai

chai.use(chaiHttp)

describe('/GET movies', () => {
  it('it should GET all the books', done => {
    chai
      .request(app)
      .get('/movies')
      .end((err, res) => {
        if (err) done(err)
        expect(res).to.have.status(200)
        expect(res.body.movieList).to.be.an('array')
        done()
      })
  })
})

describe('/POST movies', () => {
  it('it should create a movie record (imdbid: tt0106062, Matrix)', done => {
    chai
      .request(app)
      .post('/movies')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send({ i: 'tt0106062' })
      .end((err, res) => {
        if (err) done(err)
        expect(res.status).to.be.oneOf([201, 202])
        expect(res.body.movie).to.be.an('object')
        done()
      })
  })
})
