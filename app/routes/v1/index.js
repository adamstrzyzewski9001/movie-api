const express = require('express')
const Validate = require('../../validation/v1/')
const { getMovies, postMovie } = require('../../controllers/v1')
const { getComments, postComment } = require('../../controllers/v1')
const app = express()

app.get('/', (req, res) => {
  res.respond({ test: 'ok' })
})

app.get('/movies', getMovies)
app.post('/movies', Validate.post.movies, postMovie)

app.get('/comments', getComments)
app.post('/comments', Validate.post.comment, postComment)

module.exports = app
