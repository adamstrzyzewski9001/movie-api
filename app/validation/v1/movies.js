const Joi = require('@hapi/joi')
const postMovieValidationSchema = Joi.object({
  i: Joi.string(),
  t: Joi.string(),
  type: Joi.string().valid('movie', 'series', 'episode'),
  y: Joi.string()
}).or('i', 't')

module.exports = { postMovieValidationSchema }
