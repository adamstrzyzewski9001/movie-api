const Joi = require('@hapi/joi')
const postCommentValidationSchema = Joi.object({
  imdbId: Joi.string(),
  id: Joi.string(),
  author: Joi.string(),
  text: Joi.string()
}).or('imdbId', 'id')

module.exports = { postCommentValidationSchema }
