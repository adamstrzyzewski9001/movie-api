const { postMovieValidationSchema } = require('./movies')
const { postCommentValidationSchema } = require('./comments')

const Validate = {
  get: {},
  post: {}
}

Validate.post.movies = (req, res, next) => {
  const { error } = postMovieValidationSchema.validate({ ...req.body })
  if (error) {
    res.respond({}, 'wrongParams')
  } else {
    next()
  }
}

Validate.post.comment = (req, res, next) => {
  const { error } = postCommentValidationSchema.validate({ ...req.body })
  if (error) {
    res.respond({}, 'wrongParams')
  } else {
    next()
  }
}
module.exports = Validate
