const mongoose = require('mongoose')

const CommentsSchema = new mongoose.Schema({
  movieId: mongoose.SchemaTypes.ObjectId,
  movieImdbId: String,
  author: String,
  text: String,
  addTime: Date
})

const Comments = mongoose.model('Comments', CommentsSchema, 'comments')

module.exports = Comments
