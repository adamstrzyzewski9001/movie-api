const Movies = require('./Movies')
const Comments = require('./Comments')

module.exports = {
  Movies,
  Comments
}
