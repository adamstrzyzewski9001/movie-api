const mongoose = require('mongoose')

const MoviesSchema = new mongoose.Schema({
  Title: String,
  Year: String,
  Rated: String,
  Released: String,
  Runtime: String,
  Genre: String,
  Director: String,
  Writer: String,
  Actors: String,
  Plot: String,
  Language: String,
  Country: String,
  Awards: String,
  Poster: String,
  Ratings: [
    {
      Source: String,
      Value: String
    }
  ],
  Metascore: String,
  imdbRating: String,
  imdbVotes: String,
  imdbID: String,
  Type: String,
  totalSeasons: String,
  Response: String
})

const Movies = mongoose.model('Movies', MoviesSchema, 'movies')

module.exports = Movies
