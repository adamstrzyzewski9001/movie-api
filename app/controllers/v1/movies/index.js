const { Movies } = require('../../../models')
const { getMovie } = require('./request')

async function getMovies(req, res) {
  const movies = await Movies.find({}).lean()
  if (movies) {
    res.respond({ movieList: movies })
  } else {
    res.respond({}, 'serverError')
  }
}

async function postMovie(req, res) {
  const movie = await getMovie(req.body)
  if (movie) {
    const instertedMovie = await Movies.updateOne(
      { imdbID: movie.imdbID },
      movie,
      { upsert: true }
    )
    if (instertedMovie) {
      if (instertedMovie.upserted) {
        res.respond({ movie }, 'created')
      } else {
        res.respond({ movie }, 'accepted')
      }
    } else {
      res.respond({}, 'serverError')
    }
  } else {
    res.respond({ movie: {} }, 'notFound')
  }
}

module.exports = { getMovies, postMovie }
