const request = require('request-promise')
const { OMDB_API_LINK, OMDB_KEY } = process.env

async function getMovie(params) {
  const result = await request.get({
    uri: OMDB_API_LINK,
    qs: { ...params, apikey: OMDB_KEY }
  })

  if (result) {
    return JSON.parse(result) || false
  }
  return false
}

module.exports = { getMovie }
