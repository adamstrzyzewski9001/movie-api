const moment = require('moment')
const { Movies, Comments } = require('../../../models')

async function getComments(req, res) {
  const comments = await Comments.find({}).lean()
  if (comments) {
    res.respond({ comments: comments })
  } else {
    res.respond({}, 'serverError')
  }
}

async function postComment(req, res) {
  const movie = await Movies.findOne(
    {
      $or: [{ _id: req.body.id }, { imdbID: req.body.imdbId }]
    },
    { _id: 1, imdbID: 1 }
  ).lean()
  if (movie) {
    const comment = await Comments.create({
      movieId: movie._id,
      movieImdbId: movie.imdbID,
      author: req.body.author,
      text: req.body.text,
      addTime: moment()
    })
    if (comment) {
      res.respond({ comment }, 'created')
    } else {
      res.respond({}, 'serverError')
    }
  }
}

module.exports = { getComments, postComment }
