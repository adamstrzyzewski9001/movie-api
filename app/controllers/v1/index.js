const { getMovies, postMovie } = require('./movies')
const { getComments, postComment } = require('./comments')

module.exports = { getMovies, postMovie, getComments, postComment }
