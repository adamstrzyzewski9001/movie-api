FROM node:12
WORKDIR /
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 11344
CMD [ "node", "index.js" ]