require('dotenv').config()

/* Server Config Variables */
const { PORT, DB_STRING } = process.env

/* Basic modules */
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const moment = require('moment')
const uuidv4 = require('uuid/v4')

/* Customer variable (to be moved) */
const CODES = {
  success: 200,
  created: 201,
  accepted: 202,
  unauthorized: 401,
  notFound: 404,
  wrongParams: 422,
  serverError: 500
}

/* Custom file requires */
const routes = require('./app/routes/index')

/* -- Express */
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

/* -- DB Connect */
mongoose.connect(
  DB_STRING,
  { useNewUrlParser: true, useUnifiedTopology: true }
)
const db = mongoose.connection
db.on('error', err => {
  console.info('DB Connection error', err)
  process.exit()
})
db.once('open', function() {
  console.log('DB OPEN')
  app.emit('ready')
})

/* -- Middlewares */

app.use((req, res, next) => {
  res.requestStart = moment()
  res.requestId = uuidv4()

  res.respond = (response, code) => {
    res.status(CODES[code] || 200).json({
      request: {
        id: res.requestId,
        start: res.requestStart,
        end: moment(),
        time: moment().diff(res.requestStart, 'miliseconds'),
        query: req.query || {},
        body: req.body || {},
        params: req.params || {}
      },
      ...response
    })
  }
  next()
})

/* -- Routes */
app.use('/', routes)

app.on('ready', function() {
  app.listen(PORT)
})

module.exports = app
/* - Server Start */
