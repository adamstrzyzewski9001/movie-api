# Movie API

## Project assumptions

This repo holds code for a recruitment task, and is supposed to provide 4 basic
endpoints in a REST API

GET `/movies` - lists movies held in apps database

POST `/movies` - fetches details based on a movies `title` or `IMDB id` from `omdbapi.com` and saves to database

GET `/comments`- lists comments in the database

POST `/comments` - adds a comments to a specific movie held in the database, identified by it's `ObjectId` or `IMDB id`

## Setup

Steps to locally host API

1. Clone project
2. Replace `DB_STRING` string in `.env` file with a working one
3. Replace `OMDB_KEY` with your API key obtained from: `http://www.omdbapi.com/apikey.aspx`
4. Navigate into directory and run:

```
 npm install
```

5. For developement purposes you can start the app using:

```
npm run dev
```

6. In production environment you can use:

```
npm start
OR
node index.js
```

7. Optionally, you can replace `PORT` in `.env` file with your own, with the default being: `11344`
8. To issue requests locally, use the address: `localhost:11344`, for example:

```
GET localhost:11344/movies
```

## Endpoints

### GET /movies

`{app.url}/movies` - no params, return a full list of saved movies

### POST /movies

`{app.url}/movies`

#### Body Params

```
{
    "t": "matrix",
    "i": "tt0106062"
}
```

Please note, that while neither `t` or `i` is not required, you have to provide at least one of those, for the OMDB to be able to find a movie

### GET /comments

`{api.url}/comments` - no params, returns a list of saved comments

### POST /comments

`{app.url}/comments`

#### Body Params

```
{
	"imdbId": "tt0106062",
    "id":"5e2354725d0fe78997b55599",
	"author":"Adam",
	"text":"Jakiś komentarz"
}
```

Please note, that you need to provide either an `imdbId` or an `id` (which is the objectID withing the database for a movie) in order to add a comment

## Testing

You can execute a command

```
npm test
```

which will check all 4 endpoints with some basic data
